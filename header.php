<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link href="https://fonts.googleapis.com/css?family=Bree+Serif|Lato&display=swap" rel="stylesheet">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
    <div id="header">
        <div class="container">
            <a href="/">
                <h1><?= bloginfo('name'); ?></h1>
            </a>
            <p><?= bloginfo("description"); ?>
        </div>
    </div>