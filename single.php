<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

    <main id="main" class="container">

        <?php

        while ( have_posts() ) :
            the_post();
        ?> 
            <div class="meta">
                <div class="thumb">
                    <?= lazy_image(get_post_thumbnail_id()) ?>
                </div>
                <h2><?= the_title() ?></h2>
            </div>
            <div class="content">
                <?= the_content() ?>
            </div>
        <?php
            
        endwhile; 
        ?>

    </main><!-- #main -->

<?php
get_footer();
