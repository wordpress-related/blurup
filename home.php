<?php
get_header();

$posts = new WP_Query(array(
    "post_type" => "post",
));

?>
<div id="posts" class="container">
    <?php 
    if($posts->have_posts()){
        while($posts->have_posts()){
            $posts->the_post();
            ?>
            <div class="post">
                <div class='thumb'>
                    <?= lazy_image(get_post_thumbnail_id()); ?>
                </div>
                <div class="description">
                    <a href="<?= the_permalink(); ?>">
                        <h2><?= the_title() ?></h2>
                        <p><?= the_excerpt() ?></p>
                     </a>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>
<?php
get_footer();
?>